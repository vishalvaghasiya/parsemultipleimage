//
//  ViewController.swift
//  ParseMultipleImage
//
//  Created by KMSOFT on 02/06/17.
//  Copyright © 2017 VKGraphics. All rights reserved.
//

import UIKit
import Parse
import SDWebImage
class ViewController: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    
    var selectedImageView : String = ""
    var assetArray = [PFFile] ()
    var imageListArray : [imagesDetails] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let query: PFQuery = PFQuery(className: "images")
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                for object in objects! {
                    print(object.value(forKey: "img")!)
                    
                    let obj : imagesDetails = imagesDetails()
                    if let asset = object.value(forKey: "img")! as? [PFFile] {
                        for item in asset {
                            obj.image = asset
                        }
                    }
                    self.imageListArray.append(obj)
                }
                //                print(self.imageListArray?.image.index(after: 1))
            }
            else{
                print(error?.localizedDescription)
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        let objectForSave:PFObject = PFObject(className: "images")
        
        objectForSave.addObjects(from: self.assetArray, forKey: "img")
        objectForSave.saveInBackground { (success, error) in
            if error == nil {
                print("Success")
                
            }
            else{
                print(error?.localizedDescription ?? "")
            }
        }
    }
    
    @IBAction func displayImageTapped(_ sender: UIButton) {
        
        for  item in self.imageListArray {
            print(item.image!)
            print(item.image?[0].url as Any)
            self.imageVIew.sd_setImage(with: URL(string: (item.image?[0].url)!))
        }
        
    }
    
    
    @IBAction func image1ButtonTapped(_ sender: UIButton) {
        self.selectedImageView = "first"
        self.imageActionSheet(sender: sender)
    }
    
    @IBAction func image2ButtonTapped(_ sender: UIButton) {
        self.selectedImageView = "second"
        self.imageActionSheet(sender: sender)
    }
    
    @IBAction func image3ButtonTapped(_ sender: UIButton) {
        self.selectedImageView = "third"
        self.imageActionSheet(sender: sender)
    }
    
    func imageActionSheet(sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.camera()
            }
            else {
                
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    // MARK: - ImagePicker Delegate Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if(self.selectedImageView == "first") {
            
            self.imageView1.image = RBResizeImage(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!, targetSize: CGSize(width: 640, height: 480))
            do {
                let data = UIImageJPEGRepresentation(self.imageView1.image!, 0.1)
                let image = PFFile(name: "pin.jpg", data: data!)
                self.assetArray.append(image!)
            }
            catch {
                print("Error creating assets", error)
            }
        }
        else if(self.selectedImageView == "second") {
            self.imageView2.image = RBResizeImage(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!, targetSize: CGSize(width: 640, height: 480))
            do {
                let data = UIImageJPEGRepresentation(self.imageView2.image!, 0.1)
                let image = PFFile(name: "pin.jpg", data: data!)
                self.assetArray.append(image!)
            }
            catch {
                print("Error creating assets", error)
            }
        }
        else {
            self.imageView3.image = RBResizeImage(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!, targetSize: CGSize(width: 640, height: 480))
            do {
                let data = UIImageJPEGRepresentation(self.imageView3.image!, 0.1)
                let image = PFFile(name: "pin.jpg", data: data!)
                self.assetArray.append(image!)
            }
            catch {
                print("Error creating assets", error)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

