//
//  imagesDetails.swift
//  ParseMultipleImage
//
//  Created by KMSOFT on 02/06/17.
//  Copyright © 2017 VKGraphics. All rights reserved.
//

import Foundation
import Parse

class imagesDetails: PFObject , PFSubclassing {
    
    @NSManaged var image: [PFFile]?
    
    class func parseClassName() -> String {
        return "images"
    }
}
